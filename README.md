# Progressive Bot
A progressive DiscordJS v13 Bot, supporting Slash Commands and offering
essential functionality including quality of life, moderation and other
interesting commands.
