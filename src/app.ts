import { Client, Intents, Message } from 'discord.js';
import * as dotenv from 'dotenv';

const env = process.env.ENVIRONMENT ?? 'develop';
dotenv.config({ path: `./.env.${env}` });

if(!process.env.BOT_TOKEN) {
  throw "Environment undefined!";
}

const client = new Client({
  intents: [
    Intents.FLAGS.GUILDS,
    Intents.FLAGS.GUILD_MESSAGES
  ]
});

client.on('ready', () => {
  console.log('The Bot is ready.');
});

client.on('messageCreate', (message: Message) => {
  if(message.content === 'ping') {
    message.reply('png');
  }
});

client.login(process.env.BOT_TOKEN);
