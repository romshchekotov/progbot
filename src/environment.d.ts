declare global {
  namespace NodeJS {
    interface ProcessEnv {
      ENVIRONMENT?: string;
      BOT_TOKEN: string;
    }
  }
}
